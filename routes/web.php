<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
route::get('/', 'IndexController@index');

route::get('/home', 'IndexController@index');

route::get('/register', 'AuthController@register');

route::post('/welcome', 'AuthController@kirim');

route::get('/data-table', function(){
    return view('table.data-table');
});

// CRUD Cast
// Create
route::get('/cast/create', 'CastController@create'); //route untuk menuju form create
route::post('/cast','CastController@store');//menyimpan data baru ke tabel Cast

//Read data
route::get('/cast','CastController@index'); // menampilkan list data para pemain film lalu di tampilkan ke blade
route::get('/cast/{cast_id}', 'CastController@show'); // menampilkan detail data pemain film dengan id tertentu

//Update data
route::get('/cast/{cast_id}/edit', 'CastController@edit'); // menampilkan form untuk edit pemain film dengan id tertentu
route::put('/cast/{cast_id}', 'CastController@update'); // menyimpan perubahan data pemain film (update) untuk id tertentu

// Delete data
route::delete('/cast/{cast_id}', 'CastController@destroy'); // menghapus data pemain film dengan id tertentu


//route crud cast

Auth::routes();


