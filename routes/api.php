<?php


use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * route resource post
 */
// Route::get('/post', 'PostController@index');
// Route::post('/post', 'PostController@store');
// Route::put('/post', 'PostController@update');


// Route::get('/test', function(){
//     return 'masuk test api';
// });

Route::apiResource('post', 'PostController');

Route::apiResource('comment', 'CommentController');

Route::post('/auth/register','Auth\RegisterController')->name('Auth.Register');

Route::post('/auth/regenerate-otp-code','Auth\RegenerateOtpCodeController')->name('Auth.Regenerate');

