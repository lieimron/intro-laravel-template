@extends('layout.master')

@section('Judul')
<h1>Halaman Form</h1>
@endsection

@section('content')
<h2>Buat Account Baru</h2> <br>
<h3>Sign Up Form</h3> <br>
<form action="/welcome" method="post">
    @csrf
    <label for="">First Name :</label> <br><br>
    <input type="text" name="First_Name"><br><br>
          <label for="">Last Name :</label> <br><br>
    <input type="text" name="Last_Name"> <br><br>
    <label for="">Gender</label><br><br>
    <input type="radio" name="gd"> Male <br>
    <input type="radio" name="gd"> Female <br><br>
    <label for="">Nationality</label> <br><br>
    <select name="Negara" id="">
    <option value="1">Indonesia</option>
    <option value="2">Amerika</option>
    <option value="3">Inggris</option>
</select> <br><br>
    <label for="">Languange Spoken</label><br><br>
    <input type="checkbox" name="bahasa"> Bahasa Indonesia <br>
    <input type="checkbox" name="bahasa"> English <br>
    <input type="checkbox" name="bahasa"> Other <br><br>
    <label for="">Bio</label> <br><br>
    <textarea name="Bio" id="" cols="30" rows="10"></textarea> <br>
    <input type="submit" value="Sign Up">
</form>
@endsection