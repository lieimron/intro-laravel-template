@extends('layout.master')

@section('Judul')
<h1>Halaman Edit Data Pemain</h1>
@endsection

@section('content')
<form method="POST" action="/cast/{{$cast->id}}">
  @csrf
  @method("put")
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="text" name="umur" value="{{$cast->umur}}" class="form-control">
      </div>
      @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" class="form-control">{{$cast->bio}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection