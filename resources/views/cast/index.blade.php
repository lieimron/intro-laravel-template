@extends('layout.master')

@section('Judul')
<h1>Halaman List Pemain</h1>
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary mb-3" >Tambah Pemain</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
     @forelse ($cast as $key => $item)
     <tr>
         <td>{{$key + 1}}</td>
         <td>{{$item->nama}}</td>
         <td>{{$item->umur}}</td>
         <td>{{$item->bio}}</td>
         <td>
         
          
          <form class="mt-2" action="/cast/{{$item->id}}" method="POST">
          @csrf
          @method('delete')
          <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
          <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
          <input type="submit" value="Delete" class="btn btn-danger btn-sm">
          </form>

         </td>
     </tr>
         
     @empty
         <h1>Data Tidak Ada</h1>
     @endforelse
    </tbody>
  </table>

@endsection