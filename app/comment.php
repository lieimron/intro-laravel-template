<?php

namespace App;

use Illuminate\support\Str;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['conten','post_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyname()} = Str::uuid();
            }
        });
    }

    public function post()
    {
        return $this->belongeTo('App/post');
    }
}
