<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\OtpCode;
use Carbon\Carbon;


class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [
            'nama'   => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create($request->all());

        do {
            $otp = at_rand(1000, 9999);
            $check = OtpCode::where('otp', $otp)->first();

        } while($check);

        $validuntil = Carbon::now()->addMinutes(5);
        $otp_code = OtpCode::create([
            'otp' => $otp,
            'valid_until' => $validuntil,
            'user_id' => $user->id
        ]);

        Mail::to($user->user())->send(new PostAuthorMail($user));

        return response()->json([
            'success' => true,
            'message' => 'Selamat datang di Aplikasi saya. Ini adalah kode OTP Anda : '. $otp .'. Kode OTP ini berlaku 5 menit. Jangan berikan kode ini kepada siapapun.',
            'data'    => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
            ]);
    }
    
}
