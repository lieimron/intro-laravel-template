<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\OtpCode;
use Carbon\Carbon;


class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest , [
            'email'   => 'required'
            
        ]);
        
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();
        if (!$user->otpCode)
        {
            $user->otpCode->delete();
        }
        

        do {
            $otp = at_rand(1000, 9999);
            $check = OtpCode::where('otp', $otp)->first();

        } while($check);

        $validuntil = Carbon::now()->addMinutes(5);
        $otp_code = OtpCode::create([
            'otp' => $otp,
            'valid_until' => $validuntil,
            'user_id' => $user->id
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Ini adalah kode OTP Anda : '. $otp .' Kode OTP ini berlaku 5 menit. Jangan berikan kode ini kepada siapapun ',
            'data'    => [
                'user' => $user,
                'otp_code' => $otp_code,
            ]
            ]);
    }
}
