<?php

namespace App;

use Illuminate\support\Str;
use Illuminate\Database\Eloquent\Model;

class OtpCode extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyname()} = Str::uuid();
            }
        });
    }

    public function users()
    {
        return $this->belongsTo('App\Users');
    }

    public function OtpCode()
    {
        return $this->hasOne('App\OtpCode');
    }
}
